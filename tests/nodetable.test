<?php

/**
 * @file Tests.
 */

class NodetableTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Nodetable tests'),
      'description' => t('Test basic node table functionality.'),
      'group' => t('Nodetable'),
    );
  }

  function setUp() {
    parent::setUp('nodetable');
    $web_user = $this->drupalCreateUser(array('create tables'));
    $this->drupalLogin($web_user);
  }

  /**
   * Test if we can save a row properly.
   */
  function testRowCreate() {
    $nid = mt_rand(0, 100);
    $count = mt_rand(0, 10);
    $row = $this->nodetableRandomRow($nid, NODETABLE_CELL_HEADER, $count);

    $rows = _nodetable_rows($nid);
    $rid = array_pop($rows['rid']);
    $testrow = nodetable_row_load($rid);
    foreach ($testrow->cell as $testcell) {
      $cell = array_pop($row);
      $this->assertEqual($testcell->data, $cell, t('Cell content match.'));
    }
  }

  /**
   * Test if we can properly save and then load our tables.
   */
  function testTableLoad() {
    // Create a basic table with one header and two normal rows.
    $node = $this->drupalCreateNode(array('type' => 'nodetable_node', 'promote' => 1));
    $col_count = 3;
    $row_count = 2;
    $header_orig = array();
    $rows_orig = array();

    $header_orig = $this->nodetableRandomRow($node->nid, NODETABLE_CELL_HEADER, $col_count);
    for ($i = 0; $i < $row_count; $i++) {
      $rows_orig[] = $this->nodetableRandomRow($node->nid, NODETABLE_CELL_NORMAL, $col_count);
    }

    $testnode = node_load($node->nid);
    $header = $testnode->table->header;

    // Test the header.
    // Test if it is set.
    $this->assertTrue(is_array($testnode->table->header), t('Header loaded.'));
    // Test if the value is retained. The index is the key in header so we need to trick a bit.
    foreach ($header_orig as $k => $header_cell_orig) {
      $cell = array_pop($header);
      $this->assertEqual($header_cell_orig, $cell, t('Header cell nr @count matches.', array('@count' => $k)));
    }

    // Test the rows.
    // First test if they are set correctly.
    $this->assertTrue(is_array($testnode->table->rows), t('Table rows.'));
    // And they are the correct number.
    $rows = _nodetable_rows($testnode->nid);
    $this->assertEqual(count($rows), $row_number, t('Table row count matches using direct loading.'));
    $this->assertEqual(count($testnode->table->rows), $row_number, t('Table row count matches using proper node load.'));

    // And they all have the correct number of cells set.
    foreach ($rows_orig as $i => $row) {
      foreach ($row as $j => $cell) {
        $this->assertEqual($cell[$j], $testnode->table->rows[$i][$j], t('Table cell number @cellcount in row @rowcount matches.', array('@cellcount' => $j, '@rowcount' => $i)));
      }
    }

    // Add another header row and check if first header row is still there as normal cell.
  }

  /**
   * Helper function to create a table row
   */
  function nodetableCreateRow($nid, $type, $cells) {
    $row = new stdClass();
    $row->nid = $nid;
    $row->cell = array();

    // Prepare the correct row and cell structure.
    foreach ($cells as $content) {
      $cell = new stdClass();
      $cell->type = $type;
      $cell->data = $content;
      $row->cell[] = $cell;
    }
    nodetable_row_save($row);
  }

  /**
   * Create a random row with given length and return the array structure for reference.
   */
  function nodetableRandomRow($nid, $type, $count) {
    $row = array();
    for ($i = 0; $i < $count; $i++) {
      $row[] = $this->randomName();
    }
    $this->nodetableCreateRow($nid, $type, $row);
    return $row;
  }
}
